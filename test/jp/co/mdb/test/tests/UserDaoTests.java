package jp.co.mdb.test.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import jp.co.mdb.dao.User;
import jp.co.mdb.dao.UsersDao;

@ActiveProfiles("dev")
@ContextConfiguration(locations = { "classpath:jp/co/mdb/config/dao-context.xml",
		"classpath:jp/co/mdb/config/security-context.xml", 
		"classpath:jp/co/mdb/test/config/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class UserDaoTests {

	@Autowired
	private UsersDao usersDao;
	
	@Autowired
	private DataSource dataSource;
	
	private User user1 = new User("user1", "secret", "user1@example.com", true, "ROLE_USER", "User1");
	private User user2 = new User("user2", "secret", "user2@example.com", true, "ROLE_USER", "User2");
	private User user3 = new User("user3", "secret", "user3@example.com", true, "ROLE_USER", "User3");
	private User user4 = new User("user4", "secret", "user4@example.com", true, "ROLE_USER", "User4");
	
	@Before
	public void init(){
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		
		jdbc.execute("delete from offers");
		jdbc.execute("delete from messages");
		jdbc.execute("delete from users");
	}
	
	@Test
	public void testCreateRetrieve() {
		
		usersDao.create(user1);
		List<User> users1 = usersDao.getAllUsers();
		assertEquals("One user should have been created and retrieved", 1, users1.size());
		assertEquals("Insert user should match retrieved", user1, users1.get(0));
		
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);
		List<User> users2 = usersDao.getAllUsers();
		assertEquals("Should be four retrieved uses.", 4, users2.size());
		
		
		
	}
	
	@Test
	public void testExists() {
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);
		
		assertFalse("User should not exist", usersDao.exists("kdafdkad"));
		
		assertTrue("User should exist", usersDao.exists(user1.getUsername()));
		assertTrue("User should exist", usersDao.exists(user2.getUsername()));
		assertTrue("User should exist", usersDao.exists(user3.getUsername()));
		assertTrue("User should exist", usersDao.exists(user4.getUsername()));
	}
	
	/*@Test
	public void testUsers() {

		User user = new User("limon", "secret", "limon@example.com", true, "user", "Limon");
		
		usersDao.create(user);
		
		List<User> users = usersDao.getAllUsers();
		
		assertEquals("Number of users should be 1", 1, users.size());
		
		assertTrue("User should exist", usersDao.exists(user.getUsername()));
		
		
		assertEquals("Created user should be identical to retrived user", user, users.get(0));
		
	}*/
}
