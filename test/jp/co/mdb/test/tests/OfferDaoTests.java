package jp.co.mdb.test.tests;

import static org.junit.Assert.*;

import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import jp.co.mdb.dao.Offer;
import jp.co.mdb.dao.OffersDao;
import jp.co.mdb.dao.User;
import jp.co.mdb.dao.UsersDao;

@ActiveProfiles("dev")
@ContextConfiguration(locations = { "classpath:jp/co/mdb/config/dao-context.xml",
		"classpath:jp/co/mdb/config/security-context.xml", "classpath:jp/co/mdb/test/config/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class OfferDaoTests {

	@Autowired
	private OffersDao offersDao;

	@Autowired
	private UsersDao usersDao;

	@Autowired
	private DataSource dataSource;
	
	private User user1 = new User("user1", "secret", "user1@example.com", true, "ROLE_USER", "User1");
	private User user2 = new User("user2", "secret", "user2@example.com", true, "ROLE_USER", "User2");
	private User user3 = new User("user3", "secret", "user3@example.com", true, "ROLE_USER", "User3");
	private User user4 = new User("user4", "secret", "user4@example.com", false, "ROLE_USER", "User4");
	
	private Offer offer1 = new Offer(user1, "This offer from test");
	private Offer offer2 = new Offer(user1, "This offer from test");
	private Offer offer3 = new Offer(user2, "This offer from test");
	private Offer offer4 = new Offer(user3, "This offer from test");
	private Offer offer5 = new Offer(user3, "This offer from test");
	private Offer offer6 = new Offer(user3, "This offer from test");
	private Offer offer7 = new Offer(user4, "This offer from test");

	@Before
	public void init() {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);

		jdbc.execute("delete from offers");
		jdbc.execute("delete from messages");
		jdbc.execute("delete from users");
	}
	
	@Test
	public void testGetById(){
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);
		
		offersDao.saveOrUpdate(offer1);
		offersDao.saveOrUpdate(offer2);
		offersDao.saveOrUpdate(offer3);
		offersDao.saveOrUpdate(offer4);
		offersDao.saveOrUpdate(offer5);
		offersDao.saveOrUpdate(offer6);
		offersDao.saveOrUpdate(offer7);
		
		Offer retrieved1 = offersDao.getOffer(offer1.getId());
		assertEquals("Offers should match", offer1, retrieved1);
		
		Offer retrieved2 = offersDao.getOffer(offer7.getId());
		assertNull("Should not retrieve offer for diabled user.", retrieved2);
	}
	
	@Test
	public void testDelete() {
				
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);
		
		offersDao.saveOrUpdate(offer2);
		offersDao.saveOrUpdate(offer3);
		offersDao.saveOrUpdate(offer4);
		offersDao.saveOrUpdate(offer5);
		offersDao.saveOrUpdate(offer6);
		offersDao.saveOrUpdate(offer7);
		
		Offer retrieved1 = offersDao.getOffer(offer2.getId());
		assertNotNull("Offer with ID"+retrieved1.getId()+" should not be null (delete actual)", retrieved1);
		
		offersDao.delete(offer2.getId());
		Offer retrieved2 = offersDao.getOffer(offer2.getId());
		assertNull("Offer with ID "+retrieved1.getId()+" shold be null (delete actual)", retrieved2);
	}
	
	@Test
	public void testCreateRetrieved(){
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);
		
		offersDao.saveOrUpdate(offer1);
		List<Offer> offers1 = offersDao.getOffers();
		assertEquals("Should be one offer", 1, offers1.size());
		assertEquals("Retrieved offer should equal inserted offers.", offer1, offers1.get(0));
		
		offersDao.saveOrUpdate(offer2);
		offersDao.saveOrUpdate(offer3);
		offersDao.saveOrUpdate(offer4);
		offersDao.saveOrUpdate(offer5);
		offersDao.saveOrUpdate(offer6);
		offersDao.saveOrUpdate(offer7);
		
		List<Offer> offers2 = offersDao.getOffers();
		assertEquals("Should be six offers for enabled users", 6, offers2.size());
	}
	
	@Test
	public void testGetUsername() {
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);
		
		offersDao.saveOrUpdate(offer1);
		offersDao.saveOrUpdate(offer2);
		offersDao.saveOrUpdate(offer3);
		offersDao.saveOrUpdate(offer4);
		offersDao.saveOrUpdate(offer5);
		offersDao.saveOrUpdate(offer6);
		offersDao.saveOrUpdate(offer7);
		
		List<Offer> offers1 = offersDao.getOffers(user3.getUsername());
		assertEquals("Should be three offer for this user", 3, offers1.size());

		List<Offer> offers2 = offersDao.getOffers("dafdadfa");
		assertEquals("Should be zero offer for this user", 0, offers2.size());
		
		List<Offer> offers3 = offersDao.getOffers(user2.getUsername());
		assertEquals("Should be one offer for this user", 1, offers3.size());
	}
	
	@Test
	public void testUpdate() {
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);
		
		offersDao.saveOrUpdate(offer1);
		offersDao.saveOrUpdate(offer2);
		offersDao.saveOrUpdate(offer3);
		offersDao.saveOrUpdate(offer4);
		offersDao.saveOrUpdate(offer5);
		offersDao.saveOrUpdate(offer6);
		offersDao.saveOrUpdate(offer7);
		
		offer3.setText("This offer has updated text.");
		offersDao.saveOrUpdate(offer3);
		
		Offer retrieved = offersDao.getOffer(offer3.getId());
		
		assertEquals("Retrieved should be updated.", offer3, retrieved);
	}

	@Test
	public void testOffers() {

		User user = new User("limon", "secret", "limon@example.com", true, "user", "Limon");

		usersDao.create(user);

		Offer offer = new Offer(user, "This offer from test");

		offersDao.saveOrUpdate(offer);

		List<Offer> offers = offersDao.getOffers();

		assertEquals("Should be one offer in database.", 1, offers.size());

		for (Offer current : offers) {
			Offer retrieved = offersDao.getOffer(current.getId());
			
			assertEquals("Retrived offer should mathc created offer.", current, retrieved);
		}
		

		offer = offers.get(0);

		offer.setText("Update offer text.");

//		offersDao.update(offer);

//		Offer updated = offersDao.getOffer(offer.getId());
//
//		assertEquals("Updated offer should mathc retrived updated offer", offer, updated);

		offersDao.delete(offer.getId());

		List<Offer> empty = offersDao.getOffers();

		assertEquals("Offers lists should be empty", 0, empty.size());
	}
}
