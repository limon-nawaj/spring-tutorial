package jp.co.mdb.test.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import jp.co.mdb.dao.Message;
import jp.co.mdb.dao.MessagesDao;
import jp.co.mdb.dao.Offer;
import jp.co.mdb.dao.OffersDao;
import jp.co.mdb.dao.User;
import jp.co.mdb.dao.UsersDao;

@ActiveProfiles("dev")
@ContextConfiguration(locations = { "classpath:jp/co/mdb/config/dao-context.xml",
		"classpath:jp/co/mdb/config/security-context.xml", "classpath:jp/co/mdb/test/config/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class MessageDaoTest {

	@Autowired
	private OffersDao offersDao;

	@Autowired
	private UsersDao usersDao;
	
	@Autowired
	private MessagesDao messagesDao;

	@Autowired
	private DataSource dataSource;
	
	private User user1 = new User("user1", "secret", "user1@example.com", true, "ROLE_USER", "User1");
	private User user2 = new User("user2", "secret", "user2@example.com", true, "ROLE_USER", "User2");
	private User user3 = new User("user3", "secret", "user3@example.com", true, "ROLE_USER", "User3");
	private User user4 = new User("user4", "secret", "user4@example.com", false, "ROLE_USER", "User4");
	

	@Before
	public void init() {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);

		jdbc.execute("delete from offers");
		jdbc.execute("delete from messages");
		jdbc.execute("delete from users");
	}
	
	
	
	@Test
	public void testSave(){
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);
		
		Message message1 = new Message("subject", "content", "Limon", "limon@example.com", user1.getUsername());
		messagesDao.saveOrUpdate(message1);
	}
	
	
}
