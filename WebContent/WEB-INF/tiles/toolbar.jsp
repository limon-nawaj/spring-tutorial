<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<p>

	<c:choose>
		<c:when test="${hasOffer}">
			<p>
				<a href="${pageContext.request.contextPath}/offers/create">Edit
					or delete your current offer</a>
			</p>
		</c:when>
		<c:otherwise>
			<p>
				<a href="${pageContext.request.contextPath}/offers/create">Create
					Offer</a>
			</p>
		</c:otherwise>
	</c:choose>
</p>



<sec:authorize access="hasAuthority('ROLE_ADMIN')">
	<p>
		<a href="<c:url value='/admin'/>">Admin</a>
	</p>
</sec:authorize>

<sec:authorize access="isAuthenticated()">
	<p>
		<a href="<c:url value='/messages'/>">Messages (<span
			id="numberOfMessages">0</span>)
		</a>
	</p>
</sec:authorize>