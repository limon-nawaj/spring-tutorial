<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>






<h3>Create new account</h3>

<sf:form id="details" method="post"
	action="${pageContext.request.contextPath}/createaccount"
	commandName="user">

	<table class="formtable">
		<tr>
			<td class="label">Username:</td>
			<td><sf:input class="control" path="username" name="username"
					type="text" /><br /> <sf:errors path="username" cssClass="error"></sf:errors></td>
		</tr>
		
		<tr>
			<td class="label">Name:</td>
			<td><sf:input class="control" path="name" name="name"
					type="text" /><br /> <sf:errors path="name" cssClass="error"></sf:errors></td>
		</tr>
		
		<tr>
			<td class="label">Email:</td>
			<td><sf:input class="control" path="email" name="email"
					type="text" /><br /> <sf:errors path="email" cssClass="error"></sf:errors></td>
		</tr>
		<tr>
			<td class="label">Password:</td>
			<td><sf:input class="control" id="password" path="password"
					name="password" type="password" /><br /> <sf:errors
					path="password" cssClass="error"></sf:errors></td>
		</tr>
		<tr>
			<td class="label">Confirm Password:</td>
			<td><input class="control" id="confirmpass" name="confirmpass"
				type="password" /><br />
				<div id="matchpass"></div>
		</tr>
		<tr>
			<td class="label"></td>
			<td><input class="control" value="Create account" type="submit" /></td>
		</tr>
	</table>

</sf:form>
