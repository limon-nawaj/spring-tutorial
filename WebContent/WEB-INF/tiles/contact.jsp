<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>






<h3>Send Message</h3>


<sf:form method="post" commandName="message">

<input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}" />
<input type="hidden" name="_eventId" value="send" />


	<table class="formtable">

		<tr>
			<td class="label">Your name:</td>
			<td><sf:input class="control" path="name" name="name"
					type="text" value="${fromName}" /><br /> <sf:errors path="name" cssClass="error"></sf:errors></td>
		</tr>

		<tr>
			<td class="label">Your email:</td>
			<td><sf:input class="control" path="email" name="email"
					type="text" value="${fromEmail}" /><br /> <sf:errors path="email" cssClass="error"></sf:errors></td>
		</tr>

		<tr>
			<td class="label">Subject:</td>
			<td><sf:input class="control" path="subject" name="subject"
					type="text" /><br /> <sf:errors path="subject" cssClass="error"></sf:errors></td>
		</tr>

		<tr>
			<td class="label">Your Message:</td>
			<td><sf:textarea class="control" path="content" name="content"
					type="text" /><br /> <sf:errors path="content" cssClass="error"></sf:errors></td>
		</tr>


		<tr>
			<td class="label"></td>
			<td><input class="control" value="Send" type="submit" /></td>
		</tr>
	</table>

</sf:form>
